Ext.define('Pertemuan9.store.Personnel', {
    extend: 'Ext.data.Store',

    storeId: 'personnel',
    alias: 'store.personnel',
    autoLoad: true,
    autoSync: true,
    fields: [
        'name', 'email', 'phone'
    ],

    //data: { items: [
    //    { name: 'Jean Luc', email: "jeanluc.picard@enterprise.com", phone: "555-111-1111" },
    //    { name: 'Worf',     email: "worf.moghsson@enterprise.com",  phone: "555-222-2222" },
    //    { name: 'Deanna',   email: "deanna.troi@enterprise.com",    phone: "555-333-3333" },
    //    { name: 'Data',     email: "mr.data@enterprise.com",        phone: "555-444-4444" }
    //]},

    proxy: {
        type: 'jsonp',
        api : {
            read: "http://localhost/MyApp_php/readPersonnel.php",
            update: "http://localhost/MyApp_php/updatePersonnel.php",
            destroy: "http://localhost/MyApp_php/destroyPersonnel.php",
            create: "http://localhost/MyApp_php/createPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },

    listeners: {
        beforeload: function(store, operation, e0pts){
            this.getProxy().setExtraParams({
                user_id: -1
            });
        }
    }
});
